# SPRING BOOT SECURITY JWT
### Login Response
POST : localhost:8080/jwt/auth/login<br>
![Screenshot1](screenshot/login.png)<br><br>
### Echo Response
GET : localhost:8080/jwt/echo/john<br>
![Screenshot1](screenshot/welcome.png)<br><br>
### List User Response
GET : localhost:8080/jwt/user/findall<br>
![Screenshot1](screenshot/user_findall.png)<br><br>
### Unauthorized Response
GET : localhost:8080/jwt/user/findall<br>
![Screenshot1](screenshot/unauthorized.png)<br><br>