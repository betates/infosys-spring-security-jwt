package com.infosys.security.springbootjwtsecurity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtUserDTO implements Serializable{

    private String userName;
    private String role;

}
