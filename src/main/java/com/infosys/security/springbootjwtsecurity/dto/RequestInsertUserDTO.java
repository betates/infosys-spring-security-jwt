package com.infosys.security.springbootjwtsecurity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class RequestInsertUserDTO implements Serializable {
    /**
     *
     *
     */
    private static final long serialVersionUID = 3678782082188110089L;

    public RequestInsertUserDTO() {
    }

    private String code;
    private String name;
}
