package com.infosys.security.springbootjwtsecurity.endpoint;

import com.infosys.security.springbootjwtsecurity.dto.RequestInsertUserDTO;
import com.infosys.security.springbootjwtsecurity.dto.ResponseAllData;
import com.infosys.security.springbootjwtsecurity.dto.ResponseData;
import com.infosys.security.springbootjwtsecurity.dto.UserDTO;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/user")
public interface IUserEndPoint {

    @PostMapping("/insert")
    @ResponseBody
    ResponseData insert(RequestInsertUserDTO p_RequestInsertUserDTO);

    @PutMapping("/update")
    @ResponseBody
    ResponseData update(UserDTO p_UserDTO);

    @DeleteMapping("/delete")
    @ResponseBody
    ResponseData delete(Long p_Id);

    @GetMapping("/findall")
    @ResponseBody
    ResponseAllData findAll();
}
