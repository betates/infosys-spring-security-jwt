package com.infosys.security.springbootjwtsecurity.endpoint.impl;

import com.infosys.security.springbootjwtsecurity.converter.UserConverter;
import com.infosys.security.springbootjwtsecurity.dto.RequestInsertUserDTO;
import com.infosys.security.springbootjwtsecurity.dto.ResponseAllData;
import com.infosys.security.springbootjwtsecurity.dto.ResponseData;
import com.infosys.security.springbootjwtsecurity.dto.UserDTO;
import com.infosys.security.springbootjwtsecurity.endpoint.IUserEndPoint;
import com.infosys.security.springbootjwtsecurity.entity.User;
import com.infosys.security.springbootjwtsecurity.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserEndPointImpl implements IUserEndPoint {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserConverter userConverter;

    @Override
    public ResponseData insert(RequestInsertUserDTO p_RequestInsertUserDTO) {
        userService.insert(userConverter.convert(p_RequestInsertUserDTO));
        return new ResponseData("200", "Ok");
    }

    @Override
    public ResponseData update(UserDTO p_UserDTO) {
        userService.update(userConverter.convert2(p_UserDTO));
        return new ResponseData("200", "Ok");
    }

    @Override
    public ResponseData delete(Long p_Id) {
        userService.delete(p_Id);
        return new ResponseData("200", "Ok");
    }

    @Override
    public ResponseAllData findAll() {
        ResponseAllData responseAllData = new ResponseAllData();
        List<User> userList = userService.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : userList){
            userDTOList.add(userConverter.invert(user));
        }
        responseAllData.setResponseData(new ResponseData("200", "Ok"));
        responseAllData.setContent(userDTOList);
        return responseAllData;
    }
}
