package com.infosys.security.springbootjwtsecurity.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/echo")
public interface IEchoEndPoint {

    @GetMapping("/{param}")
    String echoParam(@PathVariable("param") String p_Param);
}
