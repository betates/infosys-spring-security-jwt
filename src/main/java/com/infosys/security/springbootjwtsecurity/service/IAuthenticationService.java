package com.infosys.security.springbootjwtsecurity.service;

import com.infosys.security.springbootjwtsecurity.entity.User;

public interface IAuthenticationService {

    User login(String p_UserName);
}
