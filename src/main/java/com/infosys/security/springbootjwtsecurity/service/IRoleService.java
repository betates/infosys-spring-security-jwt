package com.infosys.security.springbootjwtsecurity.service;


import com.infosys.security.springbootjwtsecurity.entity.Role;

import java.util.List;

public interface IRoleService {

    List<Role> findAllRoles();
}
