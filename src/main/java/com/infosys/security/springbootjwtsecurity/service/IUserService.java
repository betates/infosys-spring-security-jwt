package com.infosys.security.springbootjwtsecurity.service;


import com.infosys.security.springbootjwtsecurity.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    void insert(User p_User);

    void update(User p_User);

    void delete(Long p_Id);

    List<User> findAll();

    Optional<User> findByUserName(String p_UserName);
}
