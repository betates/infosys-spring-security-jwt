package com.infosys.security.springbootjwtsecurity.service.impl;

import com.infosys.security.springbootjwtsecurity.dao.IRoleDAO;
import com.infosys.security.springbootjwtsecurity.entity.Role;
import com.infosys.security.springbootjwtsecurity.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleDAO roleDAO;

    @Override
    public List<Role> findAllRoles() {
        return roleDAO.findAll();
    }
}
