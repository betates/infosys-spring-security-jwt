package com.infosys.security.springbootjwtsecurity.dao;

import com.infosys.security.springbootjwtsecurity.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRoleDAO extends JpaRepository<Role, Long> {
}
