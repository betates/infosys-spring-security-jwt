package com.Infosys.endpoint;

import com.Infosys.dto.RequestInsertUserDTO;
import com.Infosys.dto.ResponseAllData;
import com.Infosys.dto.ResponseData;
import com.Infosys.dto.UserDTO;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
public interface IUserEndPoint {

    @PostMapping("/insert")
    @ResponseBody
    ResponseData insert(RequestInsertUserDTO p_RequestInsertUserDTO);

    @PutMapping("/update")
    @ResponseBody
    ResponseData update(UserDTO p_UserDTO);

    @DeleteMapping("/delete")
    @ResponseBody
    ResponseData delete(Long p_Id);

    @GetMapping("/findall")
    @ResponseBody
    ResponseAllData findAll();
}