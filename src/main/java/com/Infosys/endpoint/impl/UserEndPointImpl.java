package com.Infosys.endpoint.impl;

import com.Infosys.converter.UserConverter;
import com.Infosys.dto.RequestInsertUserDTO;
import com.Infosys.dto.ResponseAllData;
import com.Infosys.dto.ResponseData;
import com.Infosys.dto.UserDTO;
import com.Infosys.endpoint.IUserEndPoint;
import com.Infosys.entity.User;
import com.Infosys.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserEndPointImpl implements IUserEndPoint {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserConverter userConverter;

    @Override
    public ResponseData insert(RequestInsertUserDTO p_RequestInsertUserDTO) {
        userService.insert(userConverter.convert(p_RequestInsertUserDTO));
        return new ResponseData("200", "Ok");
    }

    @Override
    public ResponseData update(UserDTO p_UserDTO) {
        userService.update(userConverter.convert2(p_UserDTO));
        return new ResponseData("200", "Ok");
    }

    @Override
    public ResponseData delete(Long p_Id) {
        userService.delete(p_Id);
        return new ResponseData("200", "Ok");
    }

    @Override
    public ResponseAllData findAll() {
        ResponseAllData responseAllData = new ResponseAllData();
        List<User> userList = userService.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : userList){
            userDTOList.add(userConverter.invert(user));
        }
        responseAllData.setResponseData(new ResponseData("200", "Ok"));
        responseAllData.setContent(userDTOList);
        return responseAllData;
    }
}