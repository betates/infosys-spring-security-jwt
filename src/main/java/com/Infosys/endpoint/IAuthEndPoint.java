package com.Infosys.endpoint;

import com.Infosys.dto.RequestLoginDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/auth")
public interface IAuthEndPoint {

    @PostMapping("/login")
    ResponseEntity login(@RequestBody RequestLoginDTO p_RequestLoginDTO);
}