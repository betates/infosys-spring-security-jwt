package com.Infosys.excpetion;

import javax.naming.AuthenticationException;

public class JwtTokenMissingException extends AuthenticationException {

    public JwtTokenMissingException(String explanation) {
        super(explanation);
    }
}