package com.Infosys.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ResponseAllData implements Serializable {
    /**
     *
     *
     */
    private static final long serialVersionUID = 4019941543569316340L;
    private List<UserDTO> content;
    private ResponseData responseData;
}