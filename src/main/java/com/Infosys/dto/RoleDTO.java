package com.Infosys.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleDTO implements Serializable {
    /**
     *
     *
     */
    private static final long serialVersionUID = 4262245510835236517L;

    private String code;
    private String name;
}