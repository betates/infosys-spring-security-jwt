package com.Infosys.dto;

import lombok.Data;

import java.io.Serializable;
import java.lang.String;

@Data
public class RequestLoginDTO implements Serializable {

    private String userName;
    private String password;
}