package com.Infosys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData implements Serializable {
    /**
     *
     *
     */
    private static final long serialVersionUID = -8192744440342273913L;
    private String code;
    private String message;
}