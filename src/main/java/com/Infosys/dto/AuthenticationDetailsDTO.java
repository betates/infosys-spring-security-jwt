package com.Infosys.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthenticationDetailsDTO implements Serializable {
    private String platform;
    private String ipAddress;
}