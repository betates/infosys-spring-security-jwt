package com.Infosys.dao;

import com.Infosys.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUserDAO extends JpaRepository<User, Long> {

    Optional<User> findByCode(String p_Code);

    User findUserByCodeOrUserProfile_phoneNumberOrUserProfile_email(String p_UserName, String p_PhoneNumber, String p_Email);

    void delete(Long id);
}