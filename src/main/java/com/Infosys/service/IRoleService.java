package com.Infosys.service;

import com.Infosys.entity.Role;

import java.util.List;

public interface IRoleService {

    List<Role> findAllRoles();
}