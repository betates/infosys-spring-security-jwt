package com.Infosys.service;

import com.Infosys.entity.User;

public interface IAuthenticationService {
    User login(String p_Username);
}
