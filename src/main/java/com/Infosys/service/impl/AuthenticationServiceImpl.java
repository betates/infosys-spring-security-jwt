package com.Infosys.service.impl;

import com.Infosys.dao.IUserDAO;
import com.Infosys.entity.CustomUserDetails;
import com.Infosys.entity.User;
import com.Infosys.service.IAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {

    @Autowired
    private IUserDAO userDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Override
    public User login(String p_UserName) {
        User user = userDAO.findUserByCodeOrUserProfile_phoneNumberOrUserProfile_email(p_UserName, p_UserName, p_UserName);
        if(user != null) {
            LOGGER.warn("User Found");
            return new CustomUserDetails(user);
        }else {
            LOGGER.warn("User Not Found");
            return null;
        }
    }
}