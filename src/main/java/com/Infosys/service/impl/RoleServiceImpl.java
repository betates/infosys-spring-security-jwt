package com.Infosys.service.impl;

import com.Infosys.dao.IRoleDAO;
import com.Infosys.entity.Role;
import com.Infosys.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private IRoleDAO roleDAO;

    @Override
    public List<Role> findAllRoles() {
        return roleDAO.findAll();
    }
}