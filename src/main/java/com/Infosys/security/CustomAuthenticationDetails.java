package com.Infosys.security;

import com.Infosys.dto.AuthenticationDetailsDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomAuthenticationDetails extends WebAuthenticationDetails implements Serializable {

    private AuthenticationDetailsDTO authenticationDetailsDTO;

    public CustomAuthenticationDetails(HttpServletRequest request) {
        super(request);
        authenticationDetailsDTO = new AuthenticationDetailsDTO();
        authenticationDetailsDTO.setPlatform(request.getHeader("User-Agent"));
        authenticationDetailsDTO.setIpAddress(request.getHeader("X-FORWARDED-FOR"));
        if (authenticationDetailsDTO.getIpAddress() == null) {
            authenticationDetailsDTO.setIpAddress(request.getRemoteAddr());
        }
    }
}